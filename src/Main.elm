module Main exposing (main)

import Browser exposing (Document)
import Html


type alias Flags = ()
type alias Model = ()
type alias Msg = Never



init : Flags -> (Model, Cmd Msg)
init flags =
  ((), Cmd.none)

view : Model -> Document Msg
view model =
  { title = "TODO"
  , body = [ Html.text "Hello, world!" ]
  }

update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
  (model, Cmd.none)

subscriptions : Model -> Sub Msg
subscriptions model =
  Sub.none


main : Program Flags Model Msg
main =
  Browser.document
    { init = init
    , view = view
    , update = update
    , subscriptions = subscriptions
    }